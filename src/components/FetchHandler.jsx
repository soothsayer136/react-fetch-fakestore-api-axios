import React, { Component } from 'react';
import axios from 'axios';
import TodoList from './TodoList'

class FetchHandler extends Component {

    state={
        fakeStore:[]
    }
    componentDidMount() {
        this.getDataFromAPI();
    }


    getDataFromAPI() {
        let self = this;
        let url = "https://fakestoreapi.com/products"
        axios.get(url)
        .then((response) => {
            self.setState({
                fakeStore:response.data
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });
            }

    render() {
        // let todoList = this.state.todos.length >0 && this.state.todos.map((item) =>{
        //     return <TodoList data={item}/>
        // })
        return (
            <div>
            <h1>FakeStore of Fetch API</h1>
            <TodoList
                
                item ={this.state.fakeStore}
                />



            </div>
        );
    }
}

export default FetchHandler;
