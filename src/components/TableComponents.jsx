import React, { Component } from 'react';


class TableComponents extends Component {
    render() {
        
        return (
            
                <tr>
                <td style={{border: '1px solid black'}}>{this.props.itemList.id}</td>
                <td style={{border: '1px solid black'}}> {this.props.itemList.title}</td>
                <td style={{border: '1px solid black'}}> {this.props.itemList.price}</td>
                <td style={{border: '1px solid black'}}> {this.props.itemList.description}</td>
                <td style={{border: '1px solid black'}}> {this.props.itemList.category}</td>
                <td style={{border: '1px solid black'}}> <img src ={this.props.itemList.image} width="50"/></td>
                
                </tr>
            
        );
    }
}

export default TableComponents;
