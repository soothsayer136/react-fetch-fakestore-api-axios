import React, { Component } from 'react';
import TableComponents from './TableComponents.jsx'

class TodoList extends Component {
    render() {
        return (
         <div style={{display:"flex", alignItems:"center", flexDirection:"column"}}>
           <table style = {{width:500, border: '1px solid black'}}>
               <thead style={{border: '1px solid black'}}>
                   <th>ID</th>
                   <th>Title</th>
                   <th>Price</th>
                   <th>Description</th>
                   <th>Category</th>
                   <th>Image</th>
               </thead>
               <tbody>
                   {this.props.item.map((data, index) =>{
                       return <TableComponents itemList={data} key={index} />
                   })}
               </tbody>
           </table>
        </div>
            
        );
    }
}

export default TodoList;
